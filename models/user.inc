<?php
class user_active_record extends active_record
{
    protected $_table = "users";

    public $uid;
    public $cmdb_assignee_id;
    public $name;
    public $pass;
    public $mail;
    public $theme;
    public $signature;
    public $signature_format;
    public $created;
    public $access;
    public $login;
    public $status;
    public $timezone;
    public $language;
    public $picture;
    public $init;
    public $data;

    /**
     * Get the currently logged in user.
     * Will return anonymous account if fail.
     *
     * @return user_active_record
     */
    static public function current()
    {
        if (!$GLOBALS['user']->uid > 0) {
            return self::search()->where('uid', 0)->execOne();
        }
        return self::search()->where('uid', $GLOBALS['user']->uid)->execOne();
    }

    /**
     * Decide if a user has a given role.
     *
     * @param $role_name string
     *
     * @return bool
     */
    public function has_role($role_name)
    {
        $role = role_active_record::search()
            ->where('name', $role_name)
            ->execOne();
        $user_role = user_role_active_record::search()
            ->where('uid', $this->uid)
            ->where('rid', $role->rid)
            ->execOne();
        if ($user_role instanceof user_role_active_record) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Decide if the user is an Administrator or not.
     *
     * @return bool
     */
    public function is_admin()
    {
        $drupal_user = user_load($this->uid);
        if (in_array('administrator', array_values($drupal_user->roles))) {
            return true;
        }
        return false;
    }

    /**
     * Get the roles of the user
     * @return Array of role_active_record
     */
    public function get_roles()
    {
        $users_roles = user_role_active_record::search()->where('uid', $this->uid)->exec();
        $roles = array();
        foreach ($users_roles as $users_role) {
            $roles[] = role_active_record::search()->where('rid', $users_role->rid)->execOne();
        }
        return $roles;
    }
}